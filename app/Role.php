<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    //

    //declara el varios a varios con usuarios
    public function users(){
        return $this
        ->belongsToMany('App\User')
        ->withTimestamps();
    }
}
