<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        //aplica la regla de acceso. esto debe cambiar con el middle
        //manda un error 401 porque no se ha definido algo especial
        $request->user()->authorizeRoles(['guess','user', 'admin']);

        return view('welcome');
    }
}
