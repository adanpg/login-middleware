<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/* NOTAS
a la autenticacion por fltro en el controlador solo se egurega y configura el middle
y el unico cambio sobre lo anterior es agregar (->middleware('auth', 'role:admin');)
en la ruta donde se quiere que aplique.

MIDDLE
la autenticacion debe usar middleware con una estructura de user-rol-roluser, 
ya que se pretende que los usuarios puedan tener mas de un rol. el ejempo de 
juan diaz funciona bien.

middleware: define a que objetos se tiene acceso
funciones: las funciones en modelos definen los detalles del acceso
ruta: implementa el middleware
kernel: declara al middleware en el sistema


*/

Route::get('/', function () {
    return view('auth/login');
});

Route::get("/otra",function () {
    return view('otra');
})->middleware('auth', 'role:admin')->name('homex');



Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
